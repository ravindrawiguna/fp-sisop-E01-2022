#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

// our header
#include "stringUtil.h"
#define PORT 8080
  
int main(int argc, char const *argv[]) {
    // Loop of argv
    printf("we got %d argument\n", argc);
    for(int i = 0;i<argc;i++){
        printf("arg[%d]: %s\n", i, argv[i]);
    }

    pid_t pid = getpid();
    printf("this program pid is: %d\n", pid);

    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    printf("socck: %d\n", sock);

    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }else{
        printf("connection success\n");
    }

    // connection success, tell the server our pid
    char pidstr[100] = {0};
    integerToString(pid, pidstr);
    send(sock, pidstr, strlen(pidstr), 0);

    char clientBuffer[1024] = {0};
    while(strcmp(clientBuffer, "exit\n") != 0){
        printf("enter message to send to server\n");
        fgets(clientBuffer,1024,stdin);
        send(sock , clientBuffer , strlen(clientBuffer) , 0 );
        printf("message sent\n");
        // reset buffer before reading
        memset(buffer, 0, sizeof(char)*1024);
        valread = read( sock , buffer, 1024);
        printf("received:\n%s\n",buffer );
    }

    return 0;
}