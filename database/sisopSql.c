#include <stdio.h> 
#include <string.h>   //strlen 
#include <stdlib.h> 
#include <errno.h> 
#include <unistd.h>   //close 
#include <arpa/inet.h>    //close 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h>
// #include <timeval.h>
#include "uidutils.h"
#include "clientutils.h"
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros 
#define PORT 8080

int main(int argc, char const *argv[]) {
    int master_socket, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int max_sd, max_clients = 30;
    //set of socket descriptors 
    fd_set readfds;  
    struct client_struct client_socket[30] = {0};
    int sd, activity;

    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";
      
    if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(master_socket, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(master_socket, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    // if ((new_socket = accept(master_socket, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
    //     perror("accept");
    // }

    //accept the incoming connection 
    addrlen = sizeof(address);  
    puts("Waiting for connections ...");

    int i;
    // timeval tval;
    struct timeval tval;
    tval.tv_sec = 5;
    tval.tv_usec = 0;

    //a message
    char *message = "ECHO Daemon v1.0 \r\n";
    while(1)
    {  
        // printf("server running...\n");
        //clear the socket set 

        FD_ZERO(&readfds);
        // printf("socket set cleared\n");
        //add master socket to set 
        FD_SET(master_socket, &readfds);
        // printf("msocket: %d\n", master_socket);
        // printf("master added to socket\n");  
        max_sd = master_socket;  


        // printf("adding child socket to set\n");
        //add child sockets to set 
        for ( i = 0 ; i < max_clients ; i++)  
        {  
            //socket descriptor 
            sd = client_socket[i].sd;  
                 
            //if valid socket descriptor then add to read list 
            if(sd > 0)  
                FD_SET( sd , &readfds);  
                 
            //highest file descriptor number, need it for the select function 
            if(sd > max_sd)  
                max_sd = sd;  
        }  
     
        //wait for an activity on one of the sockets , timeout is NULL , 
        //so wait indefinitely
        // printf("waiting\n");
        // activity = select( max_sd + 1 , &readfds , NULL , NULL , &tval);
        activity = select( max_sd + 1 , &readfds , NULL , NULL , NULL);  
        // printf("done waiting\n");
        if ((activity < 0) && (errno!=EINTR))  
        {  
            printf("select error");  
        }  
        
        // printf("is here somehow\n");
        //If something happened on the master socket , 
        //then its an incoming connection 
        if (FD_ISSET(master_socket, &readfds))  
        {  
            // printf("something is going on\n");
            if ((new_socket = accept(master_socket, 
                    (struct sockaddr *)&address, (socklen_t*)&addrlen))<0)  
            {  
                perror("accept");  
                exit(EXIT_FAILURE);  
            }  
             
            //inform user of socket number - used in send and receive commands 
            printf("New connection , socket fd is %d , ip is : %s , port : %d\n" , new_socket , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));  
           
            //send new connection greeting message 
            if( send(new_socket, message, strlen(message), 0) != strlen(message) )  
            {  
                perror("send");  
            }  
                 
            puts("Welcome message sent successfully");  
                 
            //add new socket to array of sockets 
            for (i = 0; i < max_clients; i++)  
            {  
                //if position is empty 
                if( client_socket[i].sd == 0 )  
                {  
                    client_socket[i].sd = new_socket;  
                    printf("Adding to list of sockets as %d\n" , i);  
                         
                    break;  
                }  
            }  
        }  
        printf("looping over client\n");
        //else its some IO operation on some other socket
        for (i = 0; i < max_clients; i++)  
        {  
            sd = client_socket[i].sd;  
            // printf("%d", i);
            if (FD_ISSET( sd , &readfds))  
            {  
                //Check if it was for closing , and also read the 
                //incoming message 
                if ((valread = read( sd , buffer, 1024)) == 0)  
                {  
                    //Somebody disconnected , get his details and print 
                    getpeername(sd , (struct sockaddr*)&address , \
                        (socklen_t*)&addrlen);  
                    printf("Host disconnected , ip %s , port %d \n" , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));  
                         
                    //Close the socket and mark as 0 in list for reuse 
                    close( sd );  
                    client_socket[i].sd = 0;
                    client_socket[i].is_a_root=0;
                    client_socket[i].is_checked =0;
                }  
                     
                //Echo back the message that came in 
                else 
                {  
                    //set the string terminating NULL byte on the end 
                    //of the data read
                    
                    buffer[valread] = '\0';  
                    printf("%s\n", buffer);
                    if(client_socket[i].is_checked==0){
                        client_socket[i].is_a_root = isRoot(buffer);
                        client_socket[i].is_checked = 1;
                    }
                    
                    int lenbuff = strlen(buffer);
                    int result = send(sd , buffer , lenbuff , MSG_NOSIGNAL );  
                    if(result >= 0){
                        if(result < lenbuff){
                            printf("huh not all sended\n");
                        }
                    }else{
                        switch (errno)
                        {
                        case EPIPE:
                            /* code */
                            printf("sending on a closed connection\n");
                            break;
                        
                        default:
                            break;
                        }
                    }
                }  
            }  
        }
        // printf("\n");  
    }
    printf("D:\n");  

    return 0;
}