#pragma once
#include <string.h>

void reverseString(char *src, char *dst){
    int len = strlen(src);
    int id = 0;
    for(int i = len-1;i>=0;i--){
        dst[id] = src[i];
        id+=1;
    }
    // if the end aint \0 or \n just in case
    if(dst[id] != '\0' && dst[id] != '\n'){
        dst[id] = 0;
    }
}


void integerToString(int a, char *result){
    int id = 0;
    char temp[100] = {0};
    // extract number to string
    while(a > 0){
        temp[id] = a%10 + '0';
        id+=1;
        a = a/10;
    }
    temp[id] = 0;
    // reverse it
    reverseString(temp, result);
}

void combineTwoString(char dst[], char str1[], char str2[]){
  strcpy(dst, str1);
  strcat(dst, str2);
}

