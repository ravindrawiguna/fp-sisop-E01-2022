#pragma once
#include "stringUtil.h" 
#include <stdio.h>
#include <stdlib.h>

int isRoot(char *pidstr){
    // char pidstrfied[100]={0};
    // integerToString(pid, pidstrfied);
    // char *filename = "/proc/";
    char *filename;
    filename = malloc(sizeof(char)*256);
    combineTwoString(filename, "/proc/", pidstr);
    char *final_str;
    final_str = malloc(sizeof(char)*256);
    combineTwoString(final_str, filename, "/status");
    FILE *fp = fopen(final_str, "r");

    if (fp == NULL)
    {
        printf("Error: could not open file %s", filename);
        return 1;
    }

    // read one character at a time and
    // display it to the output
    // char ch;
    // while ((ch = fgetc(fp)) != EOF)
        // putchar(ch);
    char buffer[1024] = {0};
    int is_root = 0;
    while(1){
        fgets(buffer, 1024, fp);
        
        // printf("%s\n", buffer);
        if(buffer[0] =='U' && buffer[1]=='i'){
            // get the numberro
            //Uid:	0	0	0	0
            // printf("|5%c|6%c|7%c|\n", buffer[5], buffer[6], buffer[7]);
            if(buffer[5]=='0'){
                // 0 usually mean root please :(
                is_root = 1;
                printf("THIS PID STARTED AS ROOT\n");
            }
            break;

        }
        memset(buffer, 0, 1024);
    }


    // close the file
    fclose(fp);
    return is_root;
}